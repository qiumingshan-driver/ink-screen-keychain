from machine import Pin
from machine import Pin, SPI
led1=Pin(12,Pin.OUT)
led2=Pin(13,Pin.OUT)

led1.on()
led2.on()
hspi = SPI(1, 10000000, sck=Pin(2), mosi=Pin(3), miso=Pin(10))
cs = Pin(7, mode=Pin.OUT, value=1)


# vspi = SPI(2, baudrate=80000000, polarity=0, phase=0, bits=8, firstbit=0, sck=Pin(18), mosi=Pin(23), miso=Pin(19))
cs(0)  
hspi.write(b'12345')     # write 5 bytes on MOSI
cs(1) 